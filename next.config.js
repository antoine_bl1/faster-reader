module.exports = {
  basePath: "/faster-reader",
  assetPrefix: "/faster-reader",
  publicRuntimeConfig: {
    // Will be available on client
    prefix: "/faster-reader",
  },
};
