import React, { MouseEvent, useState, useEffect } from "react";
import {
  Theme,
  withStyles,
  createStyles,
  WithStyles,
  Slider,
  IconButton,
  Grid,
} from "@material-ui/core";
import { Pause, PlayArrow, SkipPrevious, Stop } from "@material-ui/icons";
import commonStyles from "../styles/Player.module.scss";
import Timeline from "./Timeline";

const themeStyles = (theme: Theme) =>
  createStyles({
    button: {
      fontFamily: "Roboto-Regular",
    },
  });

export interface PlayerProps extends WithStyles<typeof themeStyles> {
  textToRead: string;
  onExitBtnClicked: () => void;
}

const Player = withStyles(themeStyles)(
  ({ classes, textToRead, onExitBtnClicked }: PlayerProps) => {
    const [play, setPlay] = useState<boolean>(false);
    const [
      currentWordIndexDisplayed,
      setCurrentWordIndexDisplayed,
    ] = useState<number>(0);
    const [speedWordPerMinute, setSpeedWordPerMinute] = useState<number>(800);

    const handlePlayPauseBtnClick = (): void => {
      if (isEndOfText()) {
        setCurrentWordIndexDisplayed(0);
      }
      setPlay(!play);
    };

    const handleGoBackBtnClick = (): void => {
      setPlay(false);
      setCurrentWordIndexDisplayed(0);
    };

    const handleWordPerMinuteSliderChanging = (
      event: MouseEvent,
      newSpeed: number
    ) => {
      setSpeedWordPerMinute(newSpeed);
    };

    useEffect(() => {
      var timerID = setInterval(
        () => handleTimeToChangeWord(),
        getRefreshTimeFromNumberOfWordPerMinute(speedWordPerMinute)
      );
      return function cleanup() {
        clearInterval(timerID);
      };
    });

    const getRefreshTimeFromNumberOfWordPerMinute = (
      speedWordPerMinute: number
    ): number => {
      return 1000 / (speedWordPerMinute / 60);
    };

    const handleTimeToChangeWord = (): void => {
      if (play) {
        if (currentWordIndexDisplayed >= getWords().length) {
          setPlay(false);
          return;
        }
        setCurrentWordIndexDisplayed(currentWordIndexDisplayed + 1);
      }
    };

    // return the current word to display
    // if bound index exception return last word, or the all string if only one value
    const getWordToDisplay = (): string => {
      const words = getWords();
      if (words.length) {
        return words[currentWordIndexDisplayed] || words[words.length - 1];
      }
      return textToRead;
    };

    const isEndOfText = (): boolean => {
      const words = getWords();
      return currentWordIndexDisplayed >= words.length;
    };

    const getWords = (): Array<string> => {
      return textToRead.split(" ");
    };

    return (
      <div className={commonStyles.container}>
        <p className={commonStyles.wordDisplayed}>{getWordToDisplay()}</p>
        <div className={commonStyles.toolbarContainer}>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item xs={2}>
              <IconButton
                className={classes.button}
                onClick={handlePlayPauseBtnClick}
              >
                {play ? (
                  <Pause className={commonStyles.iconButton}></Pause>
                ) : (
                  <PlayArrow className={commonStyles.iconButton}></PlayArrow>
                )}
              </IconButton>
            </Grid>
            <Grid item xs={2}>
              <IconButton
                className={classes.button}
                onClick={handleGoBackBtnClick}
              >
                <SkipPrevious
                  className={commonStyles.iconButton}
                ></SkipPrevious>
              </IconButton>
            </Grid>
            <Grid item xs={2}>
              <IconButton className={classes.button} onClick={onExitBtnClicked}>
                <Stop className={commonStyles.iconButton}></Stop>
              </IconButton>
            </Grid>
            <Grid item xs={2}>
              <Slider
                value={speedWordPerMinute}
                min={60}
                max={2400}
                onChange={handleWordPerMinuteSliderChanging}
              />
            </Grid>
            <Grid item xs={2}>
              <p className={commonStyles.speed}>
                {speedWordPerMinute} words / minutes
              </p>
            </Grid>
          </Grid>
          <Timeline
            totalNumberOfWords={getWords().length}
            currentWordIndexDisplayed={currentWordIndexDisplayed}
            onCurrentWordIndexToDisplayChanged={(newWordIndexToDisplay) =>
              setCurrentWordIndexDisplayed(newWordIndexToDisplay)
            }
          />
        </div>
      </div>
    );
  }
);

export default Player;
