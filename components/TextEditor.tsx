import React, { BaseSyntheticEvent } from "react";
import {
  Button,
  TextField,
  Theme,
  withStyles,
  createStyles,
  WithStyles,
} from "@material-ui/core";
import classNames from "classnames";
import commonStyles from "../styles/TextEditor.module.scss";

const themeStyles = (theme: Theme) =>
  createStyles({
    button: {
      margin: "1rem",
      fontFamily: "Roboto-Regular",
    },
    textfield: {
      width: "75%",
    },
  });

export interface TextEditorProps extends WithStyles<typeof themeStyles> {
  textToRead: string;
  onTextToReadChanged: (textToRead: string) => void;
  onStartReadFastBtnClicked: () => void;
}

const TextEditor = withStyles(themeStyles)(
  ({
    classes,
    textToRead,
    onTextToReadChanged,
    onStartReadFastBtnClicked,
  }: TextEditorProps) => {
    const handleTextToReadChanging = (event: BaseSyntheticEvent): void => {
      let textToReadValue = event.target.value;
      // TODO: check the input, only text ?
      onTextToReadChanged(textToReadValue);
    };

    return (
      <div className={commonStyles.container}>
        <TextField
          className={classNames(classes.textfield, commonStyles.textToRead)}
          label="Text to read very fast"
          value={textToRead}
          multiline
          rows={20}
          variant="outlined"
          onChange={handleTextToReadChanging}
        />
        <Button
          className={classes.button}
          variant="contained"
          onClick={onStartReadFastBtnClicked}
        >
          Start the read 🚀
        </Button>
      </div>
    );
  }
);

export default TextEditor;
