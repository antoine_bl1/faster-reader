import React, { MouseEvent } from "react";
import commonStyles from "../styles/Timeline.module.scss";

export interface TimelineProps {
  totalNumberOfWords: number;
  currentWordIndexDisplayed: number;
  onCurrentWordIndexToDisplayChanged: (newWordIndexToDisplay: number) => void;
}

const Timeline = ({
  totalNumberOfWords,
  currentWordIndexDisplayed,
  onCurrentWordIndexToDisplayChanged,
}: TimelineProps) => {
  const handleProgressBarClick = (event: MouseEvent) => {
    // it's easier to assume that the full width of the progress bar
    // is the page width since it occupate the full width
    const progressBarFullWidth = window.innerWidth;
    const clickX = event.pageX;
    const clickPercent = clickX / progressBarFullWidth;
    const newWordIndexRounded = Math.round(totalNumberOfWords * clickPercent);
    onCurrentWordIndexToDisplayChanged(newWordIndexRounded);
  };

  const getViewPercent = () => {
    return Math.round((currentWordIndexDisplayed / totalNumberOfWords) * 100);
  };

  return (
    <div
    className={commonStyles.container}
    onClick={(event) => handleProgressBarClick(event)}>
      <div
        className={commonStyles.progressBar}
        style={{ width: `${getViewPercent()}%` }}
      >
        <span className={commonStyles.percent}>{getViewPercent()}%</span>
      </div>
    </div>
  );
};

export default Timeline;
