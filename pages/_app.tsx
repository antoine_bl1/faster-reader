import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import Head from "next/head";
import getConfig from "next/config";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  const { publicRuntimeConfig } = getConfig();
  const PREFIX = publicRuntimeConfig.prefix;

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#131313",
      },
      secondary: {
        main: "#131313",
      },
      divider: "#131313",
      background: {
        default: "#131313",
      },
    },
  });

  // list of fonts used in scss to be preloaded (most common format type)
  const fontsToPreload = [
    "Roboto-Thin.woff",
    "Roboto-Regular.woff",
    "Roboto-Bold.woff",
  ];

  return (
    <>
      <Head>
        <title>Faster Reader</title>
        <link rel="icon" href={PREFIX + "/favicon.png"} />
        {fontsToPreload.map((font, index) => {
          // preload fonts
          return (
            <link
              key={index}
              rel="preload"
              href={`${PREFIX}/fonts/${font}`}
              as="font"
              crossOrigin=""
            />
          );
        })}
        <meta name="viewport" content="width=device-width, user-scalable=no" />
      </Head>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
}

export default MyApp;
